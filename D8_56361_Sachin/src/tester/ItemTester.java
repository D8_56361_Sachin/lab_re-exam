/*package tester;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import cricketer.Crickter;


public class CrickterTest 
{
	
	static Scanner sc = new Scanner(System.in);
	static ArrayList<Crickter> cricket = new ArrayList<>();
	static boolean exit = false;
	
	public static void main(String[] args) 
	{
		while(!false)
		{
		System.out.println("please choose from below functionalities :");
		System.out.println("1. Add Cricketers");
		System.out.println("2. Modify Crickter's rating");
		System.out.println("3. Search Crickter by name");
		System.out.println("4. Display all Cricketers added in collection.");
		System.out.println("5. Display All Crickters in sorted form by rating.");
		System.out.println("6. Exit");
		switch(sc.nextInt())
		{
		case 1:
			System.out.println("Please provide the details of cricketers to add : name, age, emailID, phone number and rating");
			cricket.add(new Crickter(sc.next(), sc.nextInt(), sc.next(), sc.next(), sc.nextInt()));
			System.out.println("Crickter details added successfully!!");
			
		break;
		
		case 2:
			System.out.println("please enter the name of crickter");
			String name = sc.next();
			System.out.println("please enter the new rating to be provided");
			int rating = sc.nextInt();
			CrickterTest.modifyRating(name, rating);
		break;
		
		case 3:
			System.out.println("Please enter the name of crickter to search");
			String name1 = sc.next();
			CrickterTest.searchByName(name1);
		break;
		
		case 4:
			System.out.println("Displaying all the crickters in the record :");
			CrickterTest.displayAllCrickters();
		break;
		
		case 5:
			System.out.println("Displaying all the crickters in sorted manner by their ratings");
			
			Collections.sort(cricket, new Comparator<Crickter>() 
			{
				public int compare(Crickter o1, Crickter o2)
				{
					return o1.getName().compareTo(o2.getName());
				}	
			});
			
		case 6:
			exit = true;
			System.out.println("Bye Bye!!");
		break;
		
		}
		
	}
	}

	private static void displayAllCrickters() 
	{
		for(Crickter c : cricket )
		{
			System.out.println(c);
		}	
	}
	

	private static void searchByName(String name1) 
	{
		for(Crickter c : cricket )
		{
			if(c.getName().equals(name1))
			{
				System.out.println("Crickter found!!");
			System.out.println(c);
			}
			else
				System.out.println("Crickter not Found.");
		}
	}

	private static void modifyRating(String name, int rating) 
	{
		for(Crickter c : cricket ) 
		{
			if(c.getName().equals(name))
			{
				c.setRating(rating);
				System.out.println("New rating updated!!");
				System.out.println(c);
			}
			else
				System.out.println("Rating could not be updated.");
		}
	}

}*/

package tester;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import items.Items;


public class ItemTester 
{
	
	private static final ItemTester ItemsTester = null;
	static Scanner sc = new Scanner(System.in);
	static ArrayList<Items> Items = new ArrayList<>();
	static boolean exit = false;
	
	public static void main(String[] args) 
	{
		while(!false)
		{
		System.out.println("please choose from below functionalities :");
		System.out.println("1. Add Item");
		System.out.println("2. Modify Item's rating");
		System.out.println("3. Search item by name");
		System.out.println("4. Display all items added in collection.");
		System.out.println("5. calculate all product price");
		System.out.println("6. Exit");
		switch(sc.nextInt())
		{
		case 1:
			System.out.println("Please provide the details of Items to add : name,Price,Quantity");
			Items.add(new Items(sc.next(), sc.nextInt(), sc.nextInt()));
			System.out.println("item details added successfully!!");
		break;
		
		case 2:
			System.out.println("please enter the name of Item");
			String name = sc.next();
			System.out.println("please enter the new rating to be provided to Item");
			int rating = sc.nextInt();
			ItemsTester.modifyRating(name, rating);
		break;
		
		case 3:
			System.out.println("Please enter the name of Item to search");
			String name1 = sc.next();
			ItemsTester.searchByName(name1);
		break;
		
		case 4:
			System.out.println("Displaying all the Items in the record :");
			ItemsTester.displayAllItems();
		break;
		
		case 5:
			System.out.println("Total Price of all Items in the record :");
			Object i1=calculatePriceofAllItems(Items);
			System.out.println(i1);
		break;
		
	
		case 6:
			exit = true;
			System.out.println("Bye Bye!!");
		break;
	
		}
		
	}
	}

	private static Object calculatePriceofAllItems(ArrayList<items.Items> items2) {
		// TODO Auto-generated method stub
		return null;
	}

	private static void displayAllItems() 
	{
		for(Items c : Items )
		{
			System.out.println(c);
		}	
	}
	

	private static void searchByName(String name1) 
	{
		for(Items c : Items )
		{
			if(c.getName().equals(name1))
				System.out.println("Item found!!");
			else
				System.out.println("Item not Found.");
		}
	}

	private static void modifyRating(String name, int rating) 
	{
		for(Items c : Items ) 
		{
			if(c.getName().equals(name))
			{
				c.setRating(rating);
				System.out.println("New rating updated!!");
			}
			else
				System.out.println("Rating could not be updated.");
		}
	}

}

